// var $base_url = 'http://localhost:8888/mandiri/'
var $base_url = 'http://localhost/mandiri/'
$(document).ready(function () {
    $('header').load($base_url + 'components/menubar.html')
    $('footer').load($base_url + 'components/footer.html')

    $('.sliders.desktop').slick({
        dots: true,
        slidesToShow: 1,
        draggable: false,
        vertical: true,
        prevArrow: "<div class='slick-prev-arrow'></div>",
        nextArrow: "<div class='slick-next-arrow'></div>",
    });

    $('.sliders.mobile').slick({
        dots: true,
        draggable: false,
        vertical: true,
        prevArrow: "<div class='slick-prev-arrow'></div>",
        nextArrow: "<div class='slick-next-arrow'></div>",
    });
})